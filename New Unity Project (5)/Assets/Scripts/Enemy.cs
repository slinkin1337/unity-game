﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Enemy : Unit
{
    [SerializeField]
    private float timeLeft = 0.1f;
    [SerializeField]
    private int health = 2;
    public AudioClip shootSound;
    public AudioClip dieSound;
    public AudioClip damageSound;
    private float timeToDie = 1.0f;
    private Bullet bullet;
    private Fire fire;
    private SpriteRenderer sprite;
    private Animator animator;
    private Vector3 direction;
    private Vector2 size = new Vector2(7.0f, 1.0f);
    private AudioSource audio;
    private int dyingTime = 0;
    private void Start()
    {
        State = BoomState.Idle;
        direction = transform.right;
        direction *= -1.0f; 
    }
    private BoomState State
    {
        get { return (BoomState)animator.GetInteger("State"); }
        set { animator.SetInteger("State", (int)value); }
    }
    private void Awake()
    {
        sprite = GetComponentInChildren<SpriteRenderer>();
        bullet = Resources.Load<Bullet>("Bullet");
        fire = Resources.Load<Fire>("Fire");
        animator = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();
    }
    private void Update()
    {
        if (health <= 0)
        {
            if (dyingTime == 0)
            {
                audio.PlayOneShot(dieSound);
            }
            dyingTime++;
            State = BoomState.Die;
            timeToDie -= Time.deltaTime;
            if (timeToDie <= 0)
            {
                diePoint++;
                print(diePoint);
                Destroy(gameObject);
                dyingTime = 0;
            }
        }
        Collider2D[] colliders = Physics2D.OverlapBoxAll(transform.position + transform.up * 1.0f + transform.right * direction.x * 2.2f,size, 0.0f);
        if(colliders.Any(x => x.GetComponent<Her>()))
        {
            timeLeft -= Time.deltaTime;
            if (timeLeft < 0 &&  health > 0)
            {
                audio.PlayOneShot(shootSound);
                Shoot();
                timeLeft = 2.0f;
            }
        }
        if(colliders.All(x => !x.GetComponent<Her>()))
        {
            timeLeft = 0.3f;
        }
    }
    private void Shoot()
    {
        Vector3 positionBullet1 = transform.position;
        Vector3 positionBullet2 = transform.position;
        Vector3 positionFire1 = transform.position;
        Vector3 positionFire2 = transform.position;

        positionBullet1.y += 0.45f;
        positionBullet2.y += 0.68f;

        positionFire1.y += 0.48f;
        positionFire2.y += 0.72f;

        positionBullet1.x -= 1.7f;
        positionBullet2.x -= 1.7f;

        positionFire1.x -= 1.5f;
        positionFire2.x -= 1.5f;
        
        Bullet newBullet1 = Instantiate(bullet, positionBullet1, bullet.transform.rotation) as Bullet;
        Bullet newBullet2 = Instantiate(bullet, positionBullet2, bullet.transform.rotation) as Bullet;
        Fire newFire1 = Instantiate(fire, positionFire1, fire.transform.rotation) as Fire;
        Fire newFire2 = Instantiate(fire, positionFire2, fire.transform.rotation) as Fire;

        newBullet1.Direction = newBullet1.transform.right * -1.0f;
        newBullet2.Direction = newBullet2.transform.right * -1.0f;
        newBullet1.Flip = sprite.flipX;
        newBullet2.Flip = sprite.flipX;
        
        newFire1.Direction = newFire1.transform.right * -1.0f;
        newFire2.Direction = newFire2.transform.right * -1.0f;
        newFire1.Flip = sprite.flipX;
        newFire2.Flip = sprite.flipX;
    }
    public override void TakingDamage()
    {
        audio.PlayOneShot(damageSound);
        health--;
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        Bullet bullet = collider.GetComponent<Bullet>();
        if (bullet)
        {
            TakingDamage();
        }
    }
}
public enum BoomState
{
    Die,
    Idle
}

