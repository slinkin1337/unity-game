﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MovingEnemy : Unit
{
    [SerializeField]
    private float speed = 2.0f;
    [SerializeField]
    private int health = 2;
    [SerializeField]
    private float timeLeft = 2.0f;
    public AudioClip shootSound;
    public AudioClip dieSound;
    public AudioClip damageSound;
    private Bullet bullet;
    private float timeToDie = 0.5f;
    private Fire fire;
    private Rigidbody2D rigidbody;
    private SpriteRenderer sprite;
    private Animator animator;
    private Vector3 direction;
    private Vector2 size = new Vector2(3.5f, 1.0f);
    private AudioSource audio;
    private int dyingTime = 0;
    private void Start()
    {
        direction = transform.right;
    }
    private EnemyState State
    {
        get { return (EnemyState)animator.GetInteger("State"); }
        set { animator.SetInteger("State", (int)value); }
    }
    private void Awake()
    {
        animator = GetComponent<Animator>();
        sprite = GetComponentInChildren<SpriteRenderer>();
        bullet = Resources.Load<Bullet>("Bullet");
        rigidbody = GetComponent<Rigidbody2D>();
        fire = Resources.Load<Fire>("Fire");
        audio = GetComponent<AudioSource>();
    }
    private void Update()
    {
        if (health <= 0)
        {
            if (dyingTime == 0)
            {
                audio.PlayOneShot(dieSound);
            }
            dyingTime++;
            rigidbody.simulated = false;
            State = EnemyState.Die;
            timeToDie -= Time.deltaTime;
            if (timeToDie <= 0)
            {
                diePoint++;
                Destroy(gameObject);
                dyingTime = 0;
            }
        }
        else
        {
            Move();
        }
    }
    private void Shoot()
    {
        Vector3 positionBullet = transform.position;
        Vector3 positionFire = transform.position;

        positionBullet.y += 0.84f;
        positionFire.y += 0.88f;

        if (sprite.flipX)
        {
            positionBullet.x -= 0.7f;
            positionFire.x -= 0.6f;
        }
        if (!sprite.flipX)
        {
            positionBullet.x += 0.6f;
            positionFire.x += 0.6f;
        }
        Bullet newBullet = Instantiate(bullet, positionBullet, bullet.transform.rotation) as Bullet;
        Fire newFire = Instantiate(fire, positionFire, fire.transform.rotation) as Fire;

        newBullet.Parent = gameObject;
        newBullet.Direction = newBullet.transform.right * (sprite.flipX ? -1.0f : 1.0f);
        newBullet.Flip = sprite.flipX;
   
        newFire.Direction = newFire.transform.right * (sprite.flipX ? -1.0f : 1.0f);
        newFire.Flip = sprite.flipX;
        
    }
    public override void TakingDamage()
    {
        audio.PlayOneShot(damageSound);
        health--;
        rigidbody.AddForce(transform.up * 5.0f, ForceMode2D.Impulse);
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        Bullet bullet = collider.GetComponent<Bullet>();
        if (bullet)
        {
            TakingDamage();
        }
    }
    private void Move()
    {
        State = EnemyState.Run;
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position + transform.up * 1.0f + transform.right * direction.x * 0.7f, 0.1f);
        Collider2D[] collidersVisibility = Physics2D.OverlapBoxAll(transform.position + transform.up * 1.0f + transform.right * direction.x * 3.2f, size, 0.0f);

        if (colliders.Length > 0 && colliders.All(x => !x.GetComponent<Her>()) && colliders.All(x => !x.GetComponent<Bullet>()))
        {
            direction *= -1.0f;
        }
        if (collidersVisibility.Any(x => x.GetComponent<Her>()))
        {
            State = EnemyState.Idle;
            speed = 0.0f;
            timeLeft -= Time.deltaTime;
            if (timeLeft < 0 && health > 0)
            {
                audio.PlayOneShot(shootSound);
                Shoot();
                timeLeft = 2.0f;
            }
        }
        if (collidersVisibility.All(x => !x.GetComponent<Her>()))
        {
            speed = 2.0f;
            timeLeft = 0.3f;
        }
        transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, speed * Time.deltaTime);
        sprite.flipX = direction.x < 0.0f;
    }
}
public enum EnemyState
{
    Run,
    Idle,
    Die
}
