﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Unit : MonoBehaviour
{
    static public int diePoint = 0;
    public virtual void TakingDamage()
    {
        Die();
    }
    protected virtual void Die()
    {
        Destroy(gameObject);
    }
}
