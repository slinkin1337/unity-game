﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    private Transform[] hearts = new Transform[5];
    private Her her;
    private void Awake()
    {
        her = FindObjectOfType<Her>();
        for (int i = 0; i < hearts.Length; i++)
        {
            hearts[i] = transform.GetChild(i);
        }  
    }
    public void Refresh()
    {
        for (int i = 0; i < hearts.Length; i++)
        {
            if(i < her.Health)
            {
                hearts[i].gameObject.SetActive(true);
            }
            else
            {
                hearts[i].gameObject.SetActive(false);
            }
        }
    }
}
