﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private GameObject parent;
    private Vector3 direction;
    private bool flip;
    public GameObject Parent
    {
        set { parent = value; }
    }
    public Vector3 Direction
    {
        set { direction = value; }
    }
    public bool Flip
    {
        set { flip = value; }
    }
    private float speed = 0.08f;
    private SpriteRenderer sprite;
    void Start()
    {
        Destroy(gameObject, 1.5f);
    }
    private void Awake()
    {
        sprite = GetComponentInChildren<SpriteRenderer>();
    }
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, speed + Time.deltaTime);
        sprite.flipX = flip ? true : false;
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        Unit unit = collider.GetComponent<Unit>();
        if (unit && unit.gameObject != parent)
        {
            Destroy(gameObject);
        }
    }
}
