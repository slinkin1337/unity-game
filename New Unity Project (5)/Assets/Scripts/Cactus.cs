﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Cactus : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collider)
    {
        Unit unit = collider.GetComponent<Unit>();
        if(unit && unit as Her)
        {
            unit.TakingDamage(); 
        }
    }
}
