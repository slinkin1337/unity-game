﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    [SerializeField]
    private float speed = 2.0f;
    [SerializeField]
    private Transform target;
    private void Awake()
    {
        if (!target)
        {
            target = FindObjectOfType<Her>().transform;
        }
    }
    private void Update()
    {
        Vector3 position = new Vector3(target.position.x, target.position.y + 1.0f, target.position.z);
        position.z = -28.0f;
        transform.position = Vector3.Lerp(transform.position, position, speed * Time.deltaTime);
    }
}
