﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    private float speed = 0.08f;
    private Vector3 direction;
    private bool flip;
    public Vector3 Direction
    {
        set { direction = value; }
    }
    public bool Flip
    {
        set { flip = value; }
    }
    private SpriteRenderer sprite;
  
    void Start()
    {
        Destroy(gameObject, 0.1f);
    }
    private void Awake()
    {
        sprite = GetComponentInChildren<SpriteRenderer>();
    }
    void Update()
    {
        sprite.flipX = flip ? true : false;
    }
}
