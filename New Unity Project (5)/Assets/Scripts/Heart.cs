﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collider)
    {
        Her her = collider.GetComponent<Her>();
        if (her)
        {
            her.Health++;
            Destroy(gameObject);
        }
    }
}
