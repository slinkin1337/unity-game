﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Her : Unit
{
    [SerializeField]
    private int health = 5;
    public int Health
    {
        get { return health; }
        set
        {
            if (value <= 5) health = value;
            healthBar.Refresh();
        }
    }
    HealthBar healthBar;
    [SerializeField]
    private float speed = 3f;
    [SerializeField]
    private float jumpSpeed = 15f;
    [SerializeField]
    private bool isGrounded = false;
    [SerializeField]
    private float timeLeft = 0.5f;
    public AudioClip shootSound;
    public AudioClip dieSound;
    public AudioClip damageSound;
    new private Rigidbody2D rigidbody;
    private Animator animator;
    private float timeToDie = 3.0f;
    private SpriteRenderer sprite;
    private Bullet bullet;
    private Fire fire;
    private float timeToEnd = 1.5f;
    private AudioSource audio;
    private int dyingTime = 0;
    private CharState State
    {
        get { return (CharState)animator.GetInteger("State"); }
        set { animator.SetInteger("State", (int)value); }
    }
    private void Awake()
    {
        //ссылки на компоненты
        rigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sprite = GetComponentInChildren<SpriteRenderer>();
        bullet = Resources.Load<Bullet>("Bullet");
        fire = Resources.Load<Fire>("Fire");
        healthBar = FindObjectOfType<HealthBar>();
        audio = GetComponent<AudioSource>();
    }
    private void FixedUpdate()
    {
        CheckGround();
    }
    void Update()
    {
        if (Unit.diePoint == 5)
        {
            timeToEnd -= Time.deltaTime;
            if (timeToEnd <= 0)
            {
                SceneManager.LoadScene("Victory");
                Unit.diePoint = 0;
            }
        }
        if (health <= 0)
        {
            print(dyingTime);
            if (dyingTime == 0)
            {
                audio.PlayOneShot(dieSound);
            }
            dyingTime++;
            State = CharState.Die;
            timeToDie -= Time.deltaTime;
            if (timeToDie < 0)
            {
                SceneManager.LoadScene("Restart");
                dyingTime = 0;
            }
        }
        else
        {
            State = CharState.Idle;
            if (Input.GetButton("Horizontal"))
            {
                Run();
            }
            if (Input.GetButtonDown("Jump") && isGrounded)
            {
                Jump();
            }
            timeLeft -= Time.deltaTime;
            if (timeLeft < 0)
            {
                if (Input.GetButtonDown("Fire1"))
                {
                    audio.PlayOneShot(shootSound);
                    Shoot();
                    timeLeft = 0.5f;
                }
            }
        }
    }
    private void Shoot()
    {
        Vector3 positionBullet = transform.position;
        Vector3 positionFire = transform.position;
        positionBullet.y += 0.82f;
        positionFire.y += 0.87f;
        if (sprite.flipX)
        {
            positionBullet.x -= 0.7f;   
            positionFire.x -= 0.6f;
        }
        if (!sprite.flipX)
        {
            positionBullet.x += 0.6f;
            positionFire.x += 0.6f; 
        }
        Bullet newBullet = Instantiate(bullet, positionBullet, bullet.transform.rotation) as Bullet;
        Fire newFire = Instantiate(fire, positionFire, fire.transform.rotation) as Fire;

        newBullet.Parent = gameObject;
        newBullet.Direction = newBullet.transform.right * (sprite.flipX ? -1.0f : 1.0f);

        newBullet.Flip = sprite.flipX;
        newFire.Direction = newFire.transform.right * (sprite.flipX ? -1.0f : 1.0f);
        newFire.Flip = sprite.flipX;
    }
    private void Run()
    {
        Vector3 direction = transform.right * Input.GetAxis("Horizontal");
        transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, speed * Time.deltaTime);
        sprite.flipX = direction.x < 0.0f;
        if (isGrounded)
        {
            State = CharState.Run;
        }
    }
    private void Jump()
    {
        State = CharState.Jump;
        rigidbody.AddForce(transform.up * jumpSpeed, ForceMode2D.Impulse);
    }
    public override void TakingDamage()
    {
        audio.PlayOneShot(damageSound);
        Health--;
        rigidbody.velocity = Vector3.zero;
        rigidbody.AddForce(transform.up * 5.0f, ForceMode2D.Impulse);
    }
    private void CheckGround()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.3f);
        isGrounded = colliders.Length > 1;      
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        Unit unit = collider.gameObject.GetComponent<Unit>();
        Bullet bullet = collider.GetComponent<Bullet>();
        Cactus cactus = collider.GetComponent<Cactus>();
        if (bullet)
        {
            TakingDamage();
        }
    }
}
public enum CharState
{
    Idle,
    Run,
    Jump,
    Die
}

